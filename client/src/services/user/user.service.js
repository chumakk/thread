import { HttpMethod } from 'src/common/enums/enums';

class User {
  constructor({ http }) {
    this._http = http;
  }

  getUsers() {
    return this._http.load('/api/users', {
      method: HttpMethod.GET
    });
  }
}

export { User };
