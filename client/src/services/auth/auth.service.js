import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Auth {
  constructor({ http }) {
    this._http = http;
  }

  login(payload) {
    return this._http.load('/api/auth/login', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  registration(payload) {
    return this._http.load('/api/auth/register', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  forgotPassword(payload) {
    return this._http.load('/api/auth/forgot', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  resetTokenValidation(token) {
    return this._http.load(`/api/auth/reset/${token}`, {
      method: HttpMethod.GET,
      hasAuth: false
    });
  }

  resetPassword(payload, token) {
    return this._http.load(`/api/auth/reset/${token}`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  getCurrentUser() {
    return this._http.load('/api/auth/user', {
      method: HttpMethod.GET
    });
  }

  updateUser(payload) {
    return this._http.load('/api/auth/updateuser', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }
}

export { Auth };
