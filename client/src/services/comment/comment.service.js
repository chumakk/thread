import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Comment {
  constructor({ http }) {
    this._http = http;
  }

  getComment(id) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.GET
    });
  }

  getLikesOfComment(filter, id) {
    return this._http.load(`/api/comments/likes/${id}`, {
      method: HttpMethod.GET,
      query: filter
    });
  }

  addComment(payload) {
    return this._http.load('/api/comments', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  updateComment(payload) {
    return this._http.load('/api/comments/update', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  deleteComment(id) {
    return this._http.load('/api/comments/delete', {
      method: HttpMethod.DELETE,
      contentType: ContentType.JSON,
      payload: JSON.stringify({ id })
    });
  }

  likeComment(commentId) {
    return this._http.load('/api/comments/react', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({ commentId, isLike: true })
    });
  }

  dislikeComment(commentId) {
    return this._http.load('/api/comments/react', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({ commentId, isLike: false })
    });
  }
}

export { Comment };
