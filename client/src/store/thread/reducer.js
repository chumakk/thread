import { createReducer } from '@reduxjs/toolkit';
import { setPosts,
  setUsers,
  addMorePosts,
  addPost,
  setExpandedPost,
  setEditablePost,
  setEditableComment,
  setLikesOfPosts,
  setLikesOfComments } from './actions';

const initialState = {
  posts: [],
  expandedPost: null,
  editablePost: null,
  editableComment: null,
  hasMorePosts: true,
  likesOfPosts: {},
  likesOfComments: {},
  users: []
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(setUsers, (state, action) => {
    const { users } = action.payload;

    state.users = users;
  });
  builder.addCase(setPosts, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addMorePosts, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addPost, (state, action) => {
    const { post } = action.payload;

    state.posts = [post, ...state.posts];
  });
  builder.addCase(setExpandedPost, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
  builder.addCase(setEditablePost, (state, action) => {
    const { post } = action.payload;

    state.editablePost = post;
  });
  builder.addCase(setEditableComment, (state, action) => {
    const { comment } = action.payload;

    state.editableComment = comment;
  });
  builder.addCase(setLikesOfPosts, (state, action) => {
    const { likesOfPosts } = action.payload;

    state.likesOfPosts = likesOfPosts;
  });
  builder.addCase(setLikesOfComments, (state, action) => {
    const { likesOfComments } = action.payload;

    state.likesOfComments = likesOfComments;
  });
});

export { reducer };
