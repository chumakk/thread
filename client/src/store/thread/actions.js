import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService,
  user as userService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_USERS: 'thread/set-all-users',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  SET_EDITABLE_POST: 'thread/set-editable-post',
  SET_EDITABLE_COMMENT: 'thread/set-editable-comment',
  SET_LIKES_OF_POSTS: 'thread/set-likes-of-posts',
  DELETE_LIKES_OF_POST: 'thread/delete-likes-of-post',
  SET_LIKES_OF_COMMENTS: 'thread/set-likes-of-comments'
};

const setUsers = createAction(ActionType.SET_ALL_USERS, users => ({
  payload: {
    users
  }
}));

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const setEditablePost = createAction(ActionType.SET_EDITABLE_POST, post => ({
  payload: {
    post
  }
}));

const setEditableComment = createAction(ActionType.SET_EDITABLE_COMMENT, comment => ({
  payload: {
    comment
  }
}));

const setLikesOfPosts = createAction(ActionType.SET_LIKES_OF_POSTS, likesOfPosts => ({
  payload: {
    likesOfPosts
  }
}));

const setLikesOfComments = createAction(ActionType.SET_LIKES_OF_COMMENTS, likesOfComments => ({
  payload: {
    likesOfComments
  }
}));

const getUsers = () => async dispatch => {
  const users = await userService.getUsers();
  dispatch(setUsers(users));
};

const sharePost = payload => async () => postService.sharePost(payload);

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const updatePost = postData => async (dispatch, getRootState) => {
  const { id } = await postService.updatePost(postData);
  const updatedPost = await postService.getPost(id);

  const {
    posts: { posts }
  } = getRootState();
  const updated = posts.map(post => (post.id !== id ? post : updatedPost));

  dispatch(setPosts(updated));
  dispatch(setEditablePost(updatedPost));
  return updatedPost;
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const likePost = postId => async (dispatch, getRootState) => {
  const likedPost = await postService.likePost(postId);

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : likedPost));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(likedPost));
  }
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const dislikedPost = await postService.dislikePost(postId);

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : dislikedPost));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(dislikedPost));
  }
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const deletePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.deletePost(postId);
  const {
    posts: { posts }
  } = getRootState();

  const updated = posts.filter(post => post.id !== id);

  dispatch(setPosts(updated));
};

const updateComment = comment => async (dispatch, getRootState) => {
  const updatedComment = await commentService.updateComment(comment);
  const {
    posts: { expandedPost }
  } = getRootState();
  const updatedExpandedPost = { ...expandedPost,
    comments: expandedPost.comments.map(com => (com.id !== updatedComment.id ? com : updatedComment)) };
  dispatch(setExpandedPost(updatedExpandedPost));
};

const deleteComment = commentId => async (dispatch, getRootState) => {
  const { id } = await commentService.deleteComment(commentId);
  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== expandedPost.id ? post
    : { ...post, commentCount: post.commentCount - 1 }));

  const updatedExpandedPost = { ...expandedPost,
    commentCount: expandedPost.commentCount - 1,
    comments: expandedPost.comments.filter(comment => comment.id !== id) };

  dispatch(setPosts(updated));
  dispatch(setExpandedPost(updatedExpandedPost));
};

const likeComment = commentId => async (dispatch, getRootState) => {
  const updatedComment = await commentService.likeComment(commentId);

  const {
    posts: { expandedPost, expandedPost: { comments } }
  } = getRootState();
  const updated = comments.map(comment => (comment.id !== commentId ? comment : updatedComment));

  if (expandedPost) {
    dispatch(setExpandedPost({ ...expandedPost, comments: updated }));
  }
};

const dislikeComment = commentId => async (dispatch, getRootState) => {
  const updatedComment = await commentService.dislikeComment(commentId);

  const {
    posts: { expandedPost, expandedPost: { comments } }
  } = getRootState();
  const updated = comments.map(comment => (comment.id !== commentId ? comment : updatedComment));

  if (expandedPost) {
    dispatch(setExpandedPost({ ...expandedPost, comments: updated }));
  }
};

const createLikesOfPost = id => (dispatch, getRootState) => {
  const {
    posts: { likesOfPosts }
  } = getRootState();
  if (!likesOfPosts[id]) {
    const newLikesOfPosts = { ...likesOfPosts, [id]: { likes: [] } };
    dispatch(setLikesOfPosts(newLikesOfPosts));
  }
};

const getLikesOfPost = (filter, id) => async (dispatch, getRootState) => {
  dispatch(createLikesOfPost(id));
  const likes = await postService.getLikesOfPost(filter, id);
  const {
    posts: { likesOfPosts }
  } = getRootState();

  if (likesOfPosts[id]) {
    const newLikesOfPosts = { ...likesOfPosts,
      [id]: {
        likes: [...likesOfPosts[id].likes, ...likes],
        hasMoreLikesOfPost: Boolean(likes.length)
      } };

    dispatch(setLikesOfPosts(newLikesOfPosts));
  }
};

const deleteLikesOfPost = id => (dispatch, getRootState) => {
  const {
    posts: { likesOfPosts }
  } = getRootState();

  const delLikesOfPost = ({ [id]: _, ...rest }) => rest;
  const newLikesOfPosts = delLikesOfPost(likesOfPosts);
  dispatch(setLikesOfPosts(newLikesOfPosts));
};

const createLikesOfComment = id => (dispatch, getRootState) => {
  const {
    posts: { likesOfComments }
  } = getRootState();
  if (!likesOfComments[id]) {
    const newLikesOfComment = { ...likesOfComments, [id]: { likes: [] } };
    dispatch(setLikesOfComments(newLikesOfComment));
  }
};

const getLikesOfComment = (filter, id) => async (dispatch, getRootState) => {
  dispatch(createLikesOfComment(id));
  const likes = await commentService.getLikesOfComment(filter, id);
  const {
    posts: { likesOfComments }
  } = getRootState();

  if (likesOfComments[id]) {
    const newLikesOfComment = { ...likesOfComments,
      [id]: {
        likes: [...likesOfComments[id].likes, ...likes],
        hasMoreLikesOfComment: Boolean(likes.length)
      } };

    dispatch(setLikesOfComments(newLikesOfComment));
  }
};

const deleteLikesOfComment = id => (dispatch, getRootState) => {
  const {
    posts: { likesOfComments }
  } = getRootState();

  const delLikesOfComment = ({ [id]: _, ...rest }) => rest;
  const newLikesOfComment = delLikesOfComment(likesOfComments);
  dispatch(setLikesOfComments(newLikesOfComment));
};

export {
  getUsers,
  setPosts,
  sharePost,
  setUsers,
  addMorePosts,
  addPost,
  setExpandedPost,
  setEditablePost,
  setEditableComment,
  setLikesOfPosts,
  setLikesOfComments,
  deleteLikesOfPost,
  deleteLikesOfComment,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  updatePost,
  toggleExpandedPost,
  likePost,
  dislikePost,
  addComment,
  deletePost,
  updateComment,
  deleteComment,
  likeComment,
  dislikeComment,
  getLikesOfPost,
  getLikesOfComment
};
