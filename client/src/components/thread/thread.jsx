import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { threadActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import { Post, Spinner, Checkbox, Dropdown } from 'src/components/common/common';
import { ExpandedPost, SharedPostLink, AddPost, EditablePost } from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  noUserId: undefined,
  reaction: undefined,
  from: 0,
  count: 10
};

const Thread = () => {
  const { posts, hasMorePosts, expandedPost, userId, editablePost, users } = useSelector(state => ({
    users: state.posts.users,
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    editablePost: state.posts.editablePost,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [sharedPostId, setSharedPostId] = React.useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [showNoOwnPosts, setShowNoOwnPosts] = React.useState(false);
  const [showOnlyLikedPosts, setShowOnlyLikedPosts] = React.useState(false);
  const [showOnlyDislikedPosts, setShowOnlyDislikedPosts] = React.useState(false);
  const dispatch = useDispatch();

  const handleSetUsers = React.useCallback(
    usersArr => dispatch(threadActionCreator.setUsers(usersArr)),
    [dispatch]
  );

  const handleGetUsers = React.useCallback(
    () => dispatch(threadActionCreator.getUsers()),
    [dispatch]
  );

  const handleSharePost = React.useCallback(
    payload => dispatch(threadActionCreator.sharePost(payload)),
    [dispatch]
  );

  const handlePostLike = React.useCallback(
    id => dispatch(threadActionCreator.likePost(id)),
    [dispatch]
  );

  const handlePostDislike = React.useCallback(
    id => dispatch(threadActionCreator.dislikePost(id)),
    [dispatch]
  );

  const handleExpandedPostToggle = React.useCallback(
    id => dispatch(threadActionCreator.toggleExpandedPost(id)),
    [dispatch]
  );

  const handleEditPost = React.useCallback(
    post => dispatch(threadActionCreator.setEditablePost(post)),
    [dispatch]
  );

  const handlePostAdd = React.useCallback(
    postPayload => dispatch(threadActionCreator.createPost(postPayload)),
    [dispatch]
  );

  const handlePostDelete = React.useCallback(
    postId => dispatch(threadActionCreator.deletePost(postId)),
    [dispatch]
  );

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMorePosts(filtersPayload));
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowNoOwnPosts = () => {
    setShowNoOwnPosts(!showNoOwnPosts);
    postsFilter.noUserId = showNoOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count;
  };

  const toggleShowOnlyLikedPosts = () => {
    setShowOnlyLikedPosts(!showOnlyLikedPosts);
    postsFilter.reaction = showOnlyLikedPosts ? undefined : true;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count;
  };

  const toggleShowOnlyDislikedPosts = () => {
    setShowOnlyDislikedPosts(!showOnlyDislikedPosts);
    postsFilter.reaction = showOnlyDislikedPosts ? undefined : false;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count;
  };

  const getMorePosts = () => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Dropdown
          text="Filter Posts"
          icon="filter"
          labeled
          button
          closeOnBlur={false}
          className="icon"
        >
          <Dropdown.Menu>
            <Dropdown.Menu scrolling>
              <Dropdown.Item onClick={e => { e.stopPropagation(); }}>
                <Checkbox
                  toggle
                  label="Show only my posts"
                  checked={showOwnPosts}
                  disabled={showNoOwnPosts}
                  onChange={toggleShowOwnPosts}
                />
              </Dropdown.Item>
              <Dropdown.Item onClick={e => { e.stopPropagation(); }}>
                <Checkbox
                  toggle
                  label="Hide my posts"
                  disabled={showOwnPosts}
                  checked={showNoOwnPosts}
                  onChange={toggleShowNoOwnPosts}
                />
              </Dropdown.Item>
              <Dropdown.Item onClick={e => { e.stopPropagation(); }}>
                <Checkbox
                  toggle
                  label="Show only liked posts"
                  checked={showOnlyLikedPosts}
                  disabled={showOnlyDislikedPosts}
                  onChange={toggleShowOnlyLikedPosts}
                />
              </Dropdown.Item>
              <Dropdown.Item onClick={e => { e.stopPropagation(); }}>
                <Checkbox
                  toggle
                  label="Show only disliked posts"
                  disabled={showOnlyLikedPosts}
                  checked={showOnlyDislikedPosts}
                  onChange={toggleShowOnlyDislikedPosts}
                />
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown.Menu>
        </Dropdown>
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            userId={userId}
            post={post}
            onEditPost={handleEditPost}
            onDeletePost={handlePostDelete}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {editablePost && <EditablePost uploadImage={uploadImage} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
          setUsers={handleSetUsers}
          getUsers={handleGetUsers}
          users={users}
          onShare={handleSharePost}
        />
      )}
    </div>
  );
};

export default Thread;
