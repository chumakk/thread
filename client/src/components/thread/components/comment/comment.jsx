import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import {
  Comment as CommentUI,
  Label,
  Icon,
  Confirm,
  LikeList,
  Popup
} from 'src/components/common/common';
import { IconName } from 'src/common/enums/enums';
import { useSelector, useDispatch } from 'react-redux';
import { commentType } from 'src/common/prop-types/prop-types';
import { threadActionCreator } from 'src/store/actions';
import EditableComment from '../editable-comment/editable-comment';

import styles from './styles.module.scss';

const likesFilter = {
  from: 0,
  count: 5
};

const Comment = ({ comment: { id, body, createdAt, user, likeCount, dislikeCount } }) => {
  const dispatch = useDispatch();
  const { userId, editableComment, likesOfComments, hasMoreLikesOfComment } = useSelector(state => ({
    userId: state.profile.user.id,
    editableComment: state.posts.editableComment,
    likesOfComments: state.posts.likesOfComments[id]?.likes ?? [],
    hasMoreLikesOfComment: state.posts.likesOfComments[id]?.hasMoreLikesOfComment ?? true
  }));
  const [isConfirm, setConfirm] = React.useState(false);
  const handleUpdateComment = React.useCallback(
    () => dispatch(threadActionCreator.setEditableComment({ id, body, createdAt, user })),
    [dispatch, id, body, createdAt, user]
  );

  const handleDeleteComment = React.useCallback(
    () => dispatch(threadActionCreator.deleteComment(id)),
    [dispatch, id]
  );

  const handleCommentLike = React.useCallback(
    () => dispatch(threadActionCreator.likeComment(id)),
    [dispatch, id]
  );

  const handleCommentDislike = React.useCallback(
    () => dispatch(threadActionCreator.dislikeComment(id)),
    [dispatch, id]
  );

  const getMoreLikesOfComment = () => {
    dispatch(threadActionCreator.getLikesOfComment(likesFilter, id));
    likesFilter.from += likesFilter.count;
  };

  const handlePopupClose = React.useCallback(() => {
    dispatch(threadActionCreator.deleteLikesOfComment(id));
    likesFilter.from = 0;
  }, [dispatch, id]);

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      {editableComment?.id === id ? (
        <EditableComment />
      ) : (
        <CommentUI.Content>
          <CommentUI.Author as="a">{user.username}</CommentUI.Author>
          <CommentUI.Metadata>
            {getFromNowTime(createdAt)}
            {userId === user.id ? (
              <CommentUI.Actions as="span" className={styles.btnWrapper}>
                <Label
                  basic
                  size="small"
                  as="a"
                  className={styles.editComment}
                  onClick={handleUpdateComment}
                >
                  <Icon name={IconName.EDIT} />
                </Label>
                <Label basic size="small" as="a" className={styles.editComment} onClick={() => setConfirm(true)}>
                  <Icon name={IconName.DELETE} />
                </Label>
                <Confirm
                  content="Are you sure you want to delete the comment?"
                  open={isConfirm}
                  onCancel={() => setConfirm(false)}
                  onConfirm={handleDeleteComment}
                />
              </CommentUI.Actions>
            ) : null}
          </CommentUI.Metadata>
          {user.status ? (
            <div>
              <span className={styles.status}>
                {user.status}
              </span>
            </div>
          ) : null}
          <CommentUI.Text>{body}</CommentUI.Text>
          <CommentUI.Actions>
            <Popup
              hoverable
              onUnmount={handlePopupClose}
              trigger={(
                <Label
                  basic
                  size="small"
                  as="a"
                  className={styles.toolbarBtn}
                  onClick={handleCommentLike}
                >
                  <Icon name={IconName.THUMBS_UP} />
                  {likeCount}
                </Label>
              )}
            >
              <Popup.Content>
                <LikeList
                  likesList={likesOfComments}
                  getMoreLikes={getMoreLikesOfComment}
                  hasMoreLikes={hasMoreLikesOfComment}
                />
              </Popup.Content>
            </Popup>
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={handleCommentDislike}
            >
              <Icon name={IconName.THUMBS_DOWN} />
              {dislikeCount}
            </Label>
          </CommentUI.Actions>
        </CommentUI.Content>
      )}
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired
};

export default Comment;
