import * as React from 'react';
import {
  Form,
  Comment as CommentUI,
  Button,
  Segment,
  Label,
  Icon
} from 'src/components/common/common';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import {
  NotificationManager
} from 'react-notifications';
import { getFromNowTime } from 'src/helpers/helpers';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';

import styles from './styles.module.scss';

const EditableComment = () => {
  const {
    editableComment: { id, createdAt, body, user }
  } = useSelector(state => ({
    editableComment: state.posts.editableComment
  }));

  const dispatch = useDispatch();
  const [text, setText] = React.useState(body);
  const [isUpdating, setIsUpdating] = React.useState(false);

  const handleCancelUpdateComment = React.useCallback(
    () => dispatch(threadActionCreator.setEditableComment(null)),
    [dispatch]
  );

  const handleUpdateComment = async () => {
    setIsUpdating(true);
    try {
      await dispatch(threadActionCreator.updateComment({ id, body: text }));
      NotificationManager.success('Comment updated');
      handleCancelUpdateComment();
    } catch (error) {
      NotificationManager.error('Comment not updated');
    } finally {
      setIsUpdating(false);
    }
  };

  return (
    <CommentUI.Content>
      <CommentUI.Author as="a">{user.username}</CommentUI.Author>
      <CommentUI.Metadata>
        {getFromNowTime(createdAt)}
        <CommentUI.Actions as="span" className={styles.btnWrapper}>
          <Label
            basic
            size="small"
            as="a"
            className={styles.editComment}
            onClick={handleCancelUpdateComment}
          >
            <Icon name={IconName.ARROWLEFT} />
          </Label>
        </CommentUI.Actions>
      </CommentUI.Metadata>
      <Segment className={styles.formWrapper}>
        <Form onSubmit={handleUpdateComment}>
          <Form.TextArea
            name="body"
            value={text}
            onChange={ev => setText(ev.target.value)}
          />
          <Button
            className="fluid"
            color={ButtonColor.BLUE}
            type={ButtonType.SUBMIT}
            iconName={IconName.SAVE}
            isLoading={isUpdating}
            isDisabled={isUpdating}
          >
            Save
          </Button>
        </Form>
      </Segment>
    </CommentUI.Content>
  );
};

export default EditableComment;
