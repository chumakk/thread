import * as React from 'react';
import PropTypes from 'prop-types';
import { IconName, IconColor } from 'src/common/enums/enums';
import {
  NotificationManager
} from 'react-notifications';
import { Icon, Modal, Input, Label, Popup, UserShareList } from 'src/components/common/common';
import { UserShareType } from '../../../../common/prop-types/userShare';

import styles from './styles.module.scss';

const SharedPostLink = ({ postId, close, users, getUsers, setUsers, onShare }) => {
  const [isCopied, setIsCopied] = React.useState(false);
  const [sharedUsers, setSahredUsers] = React.useState([]);
  let input = React.useRef();

  const copyToClipboard = ({ target }) => {
    input.select();
    document.execCommand('copy');
    target.focus();
    setIsCopied(true);
  };

  React.useEffect(() => () => setUsers([]), [setUsers]);

  const handleShare = React.useCallback(async userId => {
    if (sharedUsers.includes(userId)) {
      NotificationManager.error('You shared with this user');
      return;
    }
    onShare({ userId, postId })
      .then(() => {
        setSahredUsers([...sharedUsers, userId]);
        NotificationManager.success('Post was sent');
      })
      .catch(e => NotificationManager.error(e.message));
  }, [postId, onShare, sharedUsers]);

  const handleGetUsers = React.useCallback(() => {
    getUsers();
  }, [getUsers]);

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {isCopied && (
          <span>
            <Icon name={IconName.COPY} color={IconColor.GREEN} />
            Copied
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'copy',
            content: 'Copy',
            onClick: copyToClipboard
          }}
          value={`${window.location.origin}/share/${postId}`}
          ref={ref => {
            input = ref;
          }}
        />
        <div className={styles.emailShareWrapper}>
          <Popup
            hoverable
            trigger={(
              <Label
                className={styles.emailShare}
                basic
                as="a"
              >
                <Icon size="large" name={IconName.MAIL} />
              </Label>
            )}
          >
            <Popup.Content>
              <UserShareList
                users={users}
                sharedUsers={sharedUsers}
                onShare={handleShare}
                getUsers={handleGetUsers}
              />
            </Popup.Content>
          </Popup>
        </div>
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  users: PropTypes.arrayOf(UserShareType).isRequired,
  getUsers: PropTypes.func.isRequired,
  setUsers: PropTypes.func.isRequired,
  onShare: PropTypes.func.isRequired
};

export default SharedPostLink;
