import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { threadActionCreator } from 'src/store/actions';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import {
  Modal,
  Form,
  Segment,
  Image,
  Button
} from 'src/components/common/common';
import {
  NotificationManager
} from 'react-notifications';
import styles from './styles.module.scss';

const EditablePost = ({ uploadImage }) => {
  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.editablePost
  }));

  const [body, setBody] = React.useState('');
  const [image, setImage] = React.useState(undefined);
  const [isUploading, setIsUploading] = React.useState(false);
  const [isUpdating, setIsUpdating] = React.useState(false);

  React.useEffect(() => {
    setBody(post.body);
    setImage(post.image);
  }, [post]);

  const handleEditablePostClose = () => dispatch(threadActionCreator.setEditablePost(null));

  const handleUpdatePost = async () => {
    setIsUpdating(true);
    try {
      const data = { id: post.id, imageId: image?.id, body };
      await dispatch(threadActionCreator.updatePost(data));
      NotificationManager.success('Post updated');
      handleEditablePostClose();
    } catch (error) {
      NotificationManager.error('Post not updated');
    } finally {
      setIsUpdating(false);
    }
  };

  const handleUpdatePhoto = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id, link }) => {
        setImage({ id, link });
      })
      .catch(() => {
        NotificationManager.error('Image not loaded');
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  const handleDeletePhoto = () => setImage(undefined);

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={handleEditablePostClose}
    >
      <Segment>
        <Form onSubmit={handleUpdatePost}>
          <div className={styles.imageContainer}>
            <div className={styles.imageWrapper}>
              <Image
                className={styles.image}
                src={
                  image?.link
                  || 'https://react.semantic-ui.com/images/wireframe/image.png'
                }
                alt="post"
              />
            </div>
            <div className={styles.btnWrapper}>
              <Button
                color={ButtonColor.TEAL}
                iconName={IconName.IMAGE}
                isLoading={isUploading}
                isDisabled={isUploading || isUpdating}
              >
                <label className={styles.btnImgLabel}>
                  Change photo
                  <input
                    name="image"
                    type="file"
                    onChange={handleUpdatePhoto}
                    hidden
                  />
                </label>
              </Button>
              <Button
                color={ButtonColor.RED}
                type={ButtonType.BUTTON}
                iconName={IconName.DELETE}
                onClick={handleDeletePhoto}
              >
                <label className={styles.btnImgLabel}>Delete photo</label>
              </Button>
            </div>
          </div>
          <Form.TextArea
            name="body"
            value={body}
            placeholder="What is the news?"
            onChange={ev => setBody(ev.target.value)}
          />
          <Button
            className="fluid"
            color={ButtonColor.BLUE}
            type={ButtonType.SUBMIT}
            iconName={IconName.SAVE}
            isLoading={isUpdating}
            isDisabled={isUpdating || isUploading}
          >
            Save
          </Button>
        </Form>
      </Segment>
    </Modal>
  );
};

EditablePost.propTypes = {
  uploadImage: PropTypes.func.isRequired
};

export default EditablePost;
