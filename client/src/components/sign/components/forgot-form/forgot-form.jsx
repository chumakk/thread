import * as React from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import {
  NotificationManager
} from 'react-notifications';
import {
  ButtonType,
  ButtonSize,
  ButtonColor,
  AppRoute
} from 'src/common/enums/enums';
import {
  Button,
  Form,
  Segment,
  Message,
  NavLink
} from 'src/components/common/common';
import styles from './styles.module.scss';

const ForgotForm = ({ onForgot }) => {
  const [email, setEmail] = React.useState('');
  const [isReseting, setIsReseting] = React.useState(false);
  const [isEmailValid, setIsEmailValid] = React.useState(true);
  const [isLinkSent, setIsLinkSent] = React.useState(false);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const handleForgotClick = () => {
    if (!isEmailValid || isReseting) {
      return;
    }
    setIsReseting(true);

    onForgot({ email }).then(() => setIsLinkSent(true)).catch(e => {
      NotificationManager.error(e.message);
      setIsReseting(false);
    });
  };

  return (
    <>
      {isLinkSent ? <h2 className={styles.title}>The link has been sent to your email</h2> : (
        <>
          <h2 className={styles.title}>Reset your password</h2>
          <Form name="loginForm" size="large" onSubmit={handleForgotClick}>
            <Segment>
              <Form.Input
                fluid
                icon="at"
                iconPosition="left"
                placeholder="Email"
                type="email"
                error={!isEmailValid}
                onChange={ev => emailChanged(ev.target.value)}
                onBlur={() => setIsEmailValid(validator.isEmail(email))}
              />
              <Button
                type={ButtonType.SUBMIT}
                color={ButtonColor.TEAL}
                size={ButtonSize.LARGE}
                isLoading={isReseting}
                isFluid
                isPrimary
              >
                Forgot
              </Button>
              <div className={styles.singInWrap}>
                Alredy with us?
                {' '}
                <NavLink exact to={AppRoute.LOGIN}>
                  Sign In
                </NavLink>
              </div>
            </Segment>
          </Form>
          <Message>
            New to us?
            {' '}
            <NavLink exact to={AppRoute.REGISTRATION}>
              Sign Up
            </NavLink>
          </Message>
        </>
      )}
    </>
  );
};

ForgotForm.propTypes = {
  onForgot: PropTypes.func.isRequired
};

export default ForgotForm;
