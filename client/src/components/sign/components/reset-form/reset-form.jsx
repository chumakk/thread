import * as React from 'react';
import PropTypes from 'prop-types';
import {
  NotificationManager
} from 'react-notifications';
import { useParams, Redirect } from 'react-router-dom';
import {
  ButtonType,
  ButtonSize,
  ButtonColor,
  AppRoute
} from 'src/common/enums/enums';
import {
  Button,
  Form,
  Segment,
  Message,
  NavLink,
  Spinner
} from 'src/components/common/common';

import styles from './styles.module.scss';

const ResetForm = ({ onReset, tokenValidion }) => {
  const { token } = useParams();
  const [password, setPassword] = React.useState('');
  const [isPasswordValid, setPasswordValid] = React.useState(true);
  const [checkPassword, setChekPassword] = React.useState('');
  const [isChekPasswordValid, setChekPasswordValid] = React.useState(true);
  const [isReseting, setIsReseting] = React.useState(false);
  const [isRedirect, setIsRedirect] = React.useState(false);
  const [IsTokenValid, setTokenValid] = React.useState(false);
  const [error, setError] = React.useState(null);

  React.useEffect(() => {
    tokenValidion(token).then(() => setTokenValid(true)).catch(e => {
      NotificationManager.error(e.message);
      setIsRedirect(true);
    });
  }, [tokenValidion, token]);

  const passwordChanged = value => {
    setPassword(value);
    setPasswordValid(true);
    setError(null);
  };

  const chekPasswordChanged = value => {
    setChekPassword(value);
    setChekPasswordValid(true);
    setError(null);
  };

  const handleResetClick = () => {
    if (password !== checkPassword) {
      setError('Password must match');
      return;
    }
    if (!isPasswordValid || !isChekPasswordValid || isReseting) {
      return;
    }
    setIsReseting(true);
    onReset({ password }, token).catch(e => {
      NotificationManager.error(e.message);
      setIsReseting(false);
    });
  };

  return (
    <>
      {isRedirect ? <Redirect to="/login" /> : null}
      {!IsTokenValid ? <Spinner /> : (
        <>
          <h2 className={styles.title}>Change your password</h2>
          <Form name="loginForm" size="large" onSubmit={handleResetClick}>
            <Segment>
              <Form.Input
                fluid
                icon="lock"
                iconPosition="left"
                placeholder="Password"
                type="password"
                onChange={ev => passwordChanged(ev.target.value)}
                error={!isPasswordValid}
                onBlur={() => setPasswordValid(Boolean(password))}
              />
              <Form.Input
                fluid
                icon="lock"
                iconPosition="left"
                placeholder="Repeat password"
                type="password"
                onChange={ev => chekPasswordChanged(ev.target.value)}
                error={!isChekPasswordValid}
                onBlur={() => setChekPasswordValid(Boolean(password))}
              />
              {error ? (
                <div className={styles.error}>
                  {error}
                </div>
              ) : null}
              <Button
                type={ButtonType.SUBMIT}
                color={ButtonColor.TEAL}
                size={ButtonSize.LARGE}
                isLoading={isReseting}
                isFluid
                isPrimary
              >
                Save
              </Button>
              <div className={styles.singInWrap}>
                Alredy with us?
                {' '}
                <NavLink exact to={AppRoute.LOGIN}>
                  Sign In
                </NavLink>
              </div>
            </Segment>
          </Form>
          <Message>
            New to us?
            {' '}
            <NavLink exact to={AppRoute.REGISTRATION}>
              Sign Up
            </NavLink>
          </Message>
        </>
      )}
    </>
  );
};

ResetForm.propTypes = {
  onReset: PropTypes.func.isRequired,
  tokenValidion: PropTypes.func.isRequired
};

export default ResetForm;
