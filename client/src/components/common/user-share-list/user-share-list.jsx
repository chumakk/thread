import * as React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Spinner, Item } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { UserShareType } from '../../../common/prop-types/userShare';

import styles from './styles.module.scss';

const UserShareList = ({ users, sharedUsers, onShare, getUsers }) => {
  React.useEffect(() => {
    getUsers();
  }, [getUsers]);

  return (
    <>
      <div className={styles.usersHeader}>Click on user to share</div>
      <div className={styles.usersWrapper}>
        {!users.length ? <Spinner /> : (
          <Item.Group>
            {users.map(user => (
              <User
                key={user.id}
                sharedUsers={sharedUsers}
                user={user}
                onShare={onShare}
              />
            ))}
          </Item.Group>
        )}
      </div>
    </>
  );
};

const User = ({ onShare, sharedUsers, user: { id, username, image } }) => (
  <Item
    className={classNames(styles.userItem, { [styles.shared]: sharedUsers.includes(id) })}
    onClick={() => onShare(id)}
  >
    <Item.Image size="mini" src={image?.link ?? DEFAULT_USER_AVATAR} />
    <Item.Content content={username ?? ''} verticalAlign="middle" />
  </Item>
);

UserShareList.propTypes = {
  users: PropTypes.arrayOf(UserShareType).isRequired,
  sharedUsers: PropTypes.arrayOf(PropTypes.string).isRequired,
  onShare: PropTypes.func.isRequired,
  getUsers: PropTypes.func.isRequired
};

User.propTypes = {
  user: UserShareType.isRequired,
  sharedUsers: PropTypes.arrayOf(PropTypes.string).isRequired,
  onShare: PropTypes.func.isRequired
};

export default UserShareList;
