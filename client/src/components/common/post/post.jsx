import * as React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label, Grid, Confirm, Popup, LikeList } from 'src/components/common/common';
import { threadActionCreator } from 'src/store/actions';

import styles from './styles.module.scss';

const likesFilter = {
  from: 0,
  count: 5
};

const Post = ({
  userId,
  post,
  onPostLike,
  onPostDislike,
  onExpandedPostToggle,
  sharePost,
  onEditPost,
  onDeletePost
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    reaction
  } = post;
  const date = getFromNowTime(createdAt);

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handleEditPost = () => onEditPost(post);
  const handleDeletePost = () => onDeletePost(id);

  const [isConfirm, setConfirm] = React.useState(false);

  const { likesOfPost, hasMoreLikesOfPost } = useSelector(state => ({
    likesOfPost: state.posts.likesOfPosts[id]?.likes ?? [],
    hasMoreLikesOfPost: state.posts.likesOfPosts[id]?.hasMoreLikesOfPost ?? true
  }));
  const dispatch = useDispatch();

  const getMoreLikesOfPost = () => {
    dispatch(threadActionCreator.getLikesOfPost(likesFilter, id));
    likesFilter.from += likesFilter.count;
  };
  const handlePopupClose = React.useCallback(() => {
    dispatch(threadActionCreator.deleteLikesOfPost(id));
    likesFilter.from = 0;
  }, [dispatch, id]);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <Grid>
            <Grid.Row>
              <Grid.Column floated="left" width={11}>
                <span className="date">
                  posted by
                  {user.username}
                  {' - '}
                  {date}
                </span>
              </Grid.Column>
              {userId === user.id ? (
                <Grid.Column floated="right" textAlign="right" className={styles.editToolbar} width={4}>
                  <Label
                    basic
                    size="small"
                    as="a"
                    className={styles.editBtn}
                    onClick={handleEditPost}
                  >
                    <Icon name={IconName.EDIT} />
                  </Label>
                  <Label
                    basic
                    size="small"
                    as="a"
                    className={styles.editBtn}
                    onClick={() => setConfirm(true)}
                  >
                    <Icon name={IconName.DELETE} />
                  </Label>
                  <Confirm
                    content="Are you sure you want to delete the post?"
                    open={isConfirm}
                    onCancel={() => setConfirm(false)}
                    onConfirm={handleDeletePost}
                  />
                </Grid.Column>
              ) : null}
            </Grid.Row>
          </Grid>
        </Card.Meta>
        <Card.Description>{body}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Popup
          hoverable
          onUnmount={handlePopupClose}
          trigger={(
            <Label
              basic
              size="small"
              as="a"
              className={classNames(styles.toolbarBtn, { [styles.react]: reaction !== null ? reaction : false })}
              onClick={handlePostLike}
            >
              <Icon name={IconName.THUMBS_UP} />
              {likeCount}
            </Label>
          )}
        >
          <Popup.Content>
            <LikeList likesList={likesOfPost} getMoreLikes={getMoreLikesOfPost} hasMoreLikes={hasMoreLikesOfPost} />
          </Popup.Content>
        </Popup>
        <Label
          basic
          size="small"
          as="a"
          className={classNames(styles.toolbarBtn, { [styles.react]: reaction !== null ? !reaction : false })}
          onClick={handlePostDislike}
        >
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.defaultProps = {
  userId: '0',
  onEditPost: () => {},
  onDeletePost: () => {}
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  userId: PropTypes.string,
  onEditPost: PropTypes.func,
  onDeletePost: PropTypes.func
};

export default Post;
