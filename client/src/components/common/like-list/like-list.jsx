import * as React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { Spinner, Item } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';

const LikeList = ({ getMoreLikes, hasMoreLikes, likesList }) => (
  <div className={styles.infiniteScrollWrapper}>
    <InfiniteScroll
      pageStart={0}
      loadMore={getMoreLikes}
      hasMore={hasMoreLikes}
      loader={<Spinner key="0" />}
    >
      {likesList.length || hasMoreLikes ? (
        <Item.Group>
          {likesList.map(like => <Like key={like.user.id} like={like} />)}
        </Item.Group>
      ) : <Item.Header className={styles.notice} align="center">No likes yet</Item.Header>}
    </InfiniteScroll>
  </div>
);

const Like = ({ like }) => (
  <Item>
    <Item.Image size="mini" src={like.user.image?.link ?? DEFAULT_USER_AVATAR} />
    <Item.Content content={like.user.username ?? ''} verticalAlign="middle" />
  </Item>
);

const likeType = PropTypes.exact({
  isLike: PropTypes.bool.isRequired,
  user: PropTypes.exact({
    id: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    image: PropTypes.exact({
      id: PropTypes.string.isRequired,
      link: PropTypes.string.isRequired
    })
  }).isRequired
});

LikeList.propTypes = {
  getMoreLikes: PropTypes.func.isRequired,
  hasMoreLikes: PropTypes.bool.isRequired,
  likesList: PropTypes.arrayOf(likeType).isRequired
};

Like.propTypes = {
  like: likeType.isRequired
};
export default LikeList;
