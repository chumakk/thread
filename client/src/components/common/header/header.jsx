import * as React from 'react';
import PropTypes from 'prop-types';
import { NotificationManager } from 'react-notifications';
import { IconName, IconSize, ButtonType, AppRoute, ButtonColor } from 'src/common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { userType } from 'src/common/prop-types/prop-types';
import {
  Button,
  Icon,
  Image,
  Grid,
  NavLink,
  Label,
  Form
} from 'src/components/common/common';

import styles from './styles.module.scss';

const Header = ({ user, onUserLogout, updateStatus }) => {
  const [isEditStatus, setIsEdit] = React.useState(false);
  const [status, setStatus] = React.useState(user.status);
  const [isUploading, setIsUploading] = React.useState(false);

  React.useEffect(() => {
    setStatus(user.status);
  }, [user]);

  const onSubmitStatus = async () => {
    setIsUploading(true);
    try {
      await updateStatus(status);
      NotificationManager.success('Status updated');
      setIsEdit(false);
    } catch (error) {
      NotificationManager.error('Failed to update status');
    } finally {
      setIsUploading(false);
    }
  };
  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <NavLink exact to={AppRoute.ROOT}>
              <div className={styles.userWrapper}>
                <Image
                  circular
                  width="45"
                  height="45"
                  src={user.image?.link ?? DEFAULT_USER_AVATAR}
                />
                {' '}
                <div>
                  <div>
                    {user.username}
                  </div>
                  <div onClick={e => e.preventDefault()}>
                    {isEditStatus
                      ? (
                        <Form>
                          <Form.TextArea
                            className={styles.statusArea}
                            name="status"
                            value={status || ''}
                            rows={2}
                            placeholder="How are you?"
                            onChange={e => setStatus(e.target.value)}
                          />
                          <div className={styles.controlsWrap}>
                            <Button
                              className={styles.control}
                              isLoading={isUploading}
                              isDisabled={isUploading}
                              iconName={IconName.SAVE}
                              color={ButtonColor.BLUE}
                              type={ButtonType.BUTTON}
                              onClick={onSubmitStatus}
                            >
                              Save
                            </Button>
                            <Button
                              className={styles.control}
                              isLoading={isUploading}
                              isDisabled={isUploading}
                              iconName={IconName.DELETE}
                              color={ButtonColor.RED}
                              type={ButtonType.BUTTON}
                              onClick={() => setIsEdit(false)}
                            >
                              Cancel
                            </Button>
                          </div>
                        </Form>
                      )
                      : (
                        <div>
                          <span className={styles.status}>{user.status}</span>
                          <Label basic size="small" className={styles.editStatus} onClick={() => setIsEdit(true)}>
                            <Icon name={IconName.PENCIL} />
                          </Label>
                        </div>
                      ) }
                  </div>
                </div>
              </div>
            </NavLink>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink
            exact
            activeClassName="active"
            to={AppRoute.PROFILE}
            className={styles.menuBtn}
          >
            <Icon name={IconName.USER_CIRCLE} size={IconSize.LARGE} />
          </NavLink>
          <Button
            className={`${styles.menuBtn} ${styles.logoutBtn}`}
            onClick={onUserLogout}
            type={ButtonType.BUTTON}
            iconName={IconName.LOG_OUT}
            iconSize={IconSize.LARGE}
            isBasic
          />
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  onUserLogout: PropTypes.func.isRequired,
  user: userType.isRequired,
  updateStatus: PropTypes.func.isRequired
};

export default Header;
