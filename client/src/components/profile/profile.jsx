import * as React from 'react';
import { useSelector } from 'react-redux';
import { Grid, Image, Input, Button, Form } from 'src/components/common/common';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import ProfileForm from './profile-form/profileForm';
import styles from './styles.module.scss';

const Profile = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  const [isEdit, setIsEdit] = React.useState(false);
  return (
    <div>
      {isEdit ? <ProfileForm onCancel={setIsEdit} /> : (
        <Grid container textAlign="center" style={{ paddingTop: 30 }}>
          <Grid.Column>
            <Image
              centered
              src={user.image?.link ?? DEFAULT_USER_AVATAR}
              size="medium"
              circular
            />
            <br />
            <Input
              icon="user"
              iconPosition="left"
              placeholder="Username"
              type="text"
              disabled
              value={user.username}
            />
            <br />
            <br />
            <Input
              icon="at"
              iconPosition="left"
              placeholder="Email"
              type="email"
              disabled
              value={user.email}
            />
            <Form className={styles.textAreaWrapper}>
              <Form.TextArea
                name="status"
                value={user.status || ''}
                rows={2}
                disabled
              />
            </Form>
            <div className={styles.toolbarWrapper}>
              <Button
                className={styles.toolbarBtn}
                iconName={IconName.EDIT}
                color={ButtonColor.TEAL}
                type={ButtonType.BUTTON}
                onClick={() => setIsEdit(true)}
              >
                Change
              </Button>
            </div>
          </Grid.Column>
        </Grid>
      )}
    </div>
  );
};

export default Profile;
