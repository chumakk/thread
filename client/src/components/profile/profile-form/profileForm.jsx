import * as React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { NotificationManager } from 'react-notifications';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import { Grid, Image, Input, Button, Form } from 'src/components/common/common';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { profileActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';

import styles from './styles.module.scss';

const ProfileForm = ({ onCancel }) => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  const dispatch = useDispatch();
  const [username, setUsername] = React.useState(user.username);
  const [image, setImage] = React.useState(user.image);
  const [status, setStatus] = React.useState(user.status);
  const [isUploading, setIsUploading] = React.useState(false);
  const [crop, setCrop] = React.useState({ aspect: 1 });
  const [completedCrop, setCompletedCrop] = React.useState(null);
  const [upImg, setUpImg] = React.useState();
  const [blobImage, setBlobImage] = React.useState(null);
  const imgRef = React.useRef(null);

  const uploadImage = file => imageService.uploadImage(file);

  const onLoad = React.useCallback(img => {
    imgRef.current = img;
  }, []);

  React.useEffect(() => {
    setStatus(user.status);
  }, [user]);

  React.useEffect(() => {
    if (!completedCrop || !imgRef.current) {
      return;
    }

    const currentImage = imgRef.current;
    const canvas = document.createElement('canvas');
    const currentCrop = completedCrop;

    const scaleX = currentImage.naturalWidth / currentImage.width;
    const scaleY = currentImage.naturalHeight / currentImage.height;
    const ctx = canvas.getContext('2d');
    const pixelRatio = window.devicePixelRatio;

    canvas.width = currentCrop.width * pixelRatio;
    canvas.height = currentCrop.height * pixelRatio;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = 'high';

    ctx.drawImage(
      currentImage,
      currentCrop.x * scaleX,
      currentCrop.y * scaleY,
      currentCrop.width * scaleX,
      currentCrop.height * scaleY,
      0,
      0,
      currentCrop.width,
      currentCrop.height
    );

    canvas.toBlob(blob => {
      if (!blob) return;
      setBlobImage(blob);
    }, 'image/jpeg');
  }, [completedCrop]);

  const handleUploadFile = () => {
    setIsUploading(true);
    uploadImage(blobImage)
      .then(({ id, link }) => {
        setImage({ id, link });
        setUpImg(null);
        setBlobImage(null);
      })
      .catch(e => {
        NotificationManager.error(e.message);
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  const handleUpdateUserData = async () => {
    const updateUser = {
      id: user.id,
      username,
      imageId: image?.id || null,
      status: status || null
    };
    try {
      await dispatch(profileActionCreator.updateUser(updateUser));
      NotificationManager.success('Profile updated');
    } catch (e) {
      NotificationManager.error(e.message);
    }
  };

  const handleCropFile = ({ target }) => {
    const [file] = target.files;
    const reader = new FileReader();
    reader.addEventListener('load', () => setUpImg(reader.result));
    reader.readAsDataURL(file);
  };

  const onReset = () => {
    setUsername(user.username);
    setImage(user.image);
    setStatus(user.status);
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <div className={styles.backToolWrapper}>
          <Button
            iconName={IconName.ARROWLEFT}
            color={ButtonColor.GREY}
            type={ButtonType.BUTTON}
            onClick={() => onCancel(false)}
          />
        </div>
        <Form onSubmit={handleUpdateUserData}>
          <Image
            centered
            src={image?.link ?? DEFAULT_USER_AVATAR}
            size="medium"
            circular
          />
          <div className={styles.cropWrapper}>
            <ReactCrop
              className={styles.cropWidth}
              src={upImg}
              onImageLoaded={onLoad}
              crop={crop}
              onChange={c => setCrop(c)}
              onComplete={c => setCompletedCrop(c)}
              maxWidth="160"
            />
          </div>
          <div className={styles.toolbarWrapper}>
            <Button
              className={styles.toolbarBtn}
              color={ButtonColor.TEAL}
              isLoading={isUploading}
              isDisabled={isUploading}
              iconName={IconName.IMAGE}
              disabled={isUploading}
            >
              <label className={styles.btnImgLabel}>
                Attach image
                <input
                  name="image"
                  type="file"
                  onChange={handleCropFile}
                  hidden
                />
              </label>
            </Button>
            <Button
              className={styles.toolbarBtn}
              iconName={IconName.UPLOAD}
              color={ButtonColor.YELLOW}
              onClick={handleUploadFile}
              isLoading={isUploading}
              isDisabled={isUploading || !blobImage}
              type={ButtonType.BUTTON}
            >
              Apply
            </Button>
            <Button
              isLoading={isUploading}
              isDisabled={isUploading
        || !(blobImage || user.image?.id !== image?.id || upImg)}
              className={styles.toolbarBtn}
              iconName={IconName.DELETE}
              onClick={() => {
                setImage(user.image);
                setBlobImage(null);
                setUpImg(undefined);
              }}
              color={ButtonColor.RED}
              type={ButtonType.BUTTON}
            >
              Cancel
            </Button>
            <Button
              isLoading={isUploading}
              isDisabled={isUploading || !image}
              className={styles.toolbarBtn}
              iconName={IconName.DELETE}
              onClick={() => setImage(null)}
              color={ButtonColor.RED}
              type={ButtonType.BUTTON}
            >
              Delete photo
            </Button>
          </div>
          <br />
          <div>
            <label>Username</label>
          </div>
          <Input
            icon="user"
            iconPosition="left"
            placeholder="Username"
            type="text"
            value={username}
            onChange={e => setUsername(e.target.value)}
          />
          <br />
          <br />
          <div className={styles.textAreaWrapper}>
            <div>
              <label>Status</label>
            </div>
            <Form.TextArea
              name="status"
              value={status || ''}
              rows={2}
              placeholder="How are you?"
              onChange={e => setStatus(e.target.value)}
            />
          </div>
          <div className={styles.toolbarWrapper}>
            <Button
              className={styles.toolbarBtn}
              isLoading={isUploading}
              isDisabled={isUploading}
              iconName={IconName.SAVE}
              color={ButtonColor.BLUE}
              type={ButtonType.SUBMIT}
            >
              Save
            </Button>
            <Button
              isLoading={isUploading}
              isDisabled={isUploading}
              className={styles.toolbarBtn}
              iconName={IconName.DELETE}
              color={ButtonColor.RED}
              type={ButtonType.BUTTON}
              onClick={onReset}
            >
              Reset
            </Button>
          </div>
        </Form>
      </Grid.Column>
    </Grid>
  );
};

ProfileForm.propTypes = {
  onCancel: PropTypes.func.isRequired
};

export default ProfileForm;
