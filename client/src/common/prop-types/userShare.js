import PropTypes from 'prop-types';

const UserShareType = PropTypes.exact({
  id: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  image: PropTypes.exact({
    link: PropTypes.string.isRequired
  })
});

export { UserShareType };
