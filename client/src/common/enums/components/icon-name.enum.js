const IconName = {
  USER_CIRCLE: 'user circle',
  LOG_OUT: 'log out',
  THUMBS_UP: 'thumbs up',
  THUMBS_DOWN: 'thumbs down',
  COMMENT: 'comment',
  SHARE_ALTERNATE: 'share alternate',
  FROWN: 'frown',
  IMAGE: 'image',
  COPY: 'copy',
  EDIT: 'edit',
  DELETE: 'delete',
  SAVE: 'save',
  ARROWLEFT: 'arrow left',
  UPLOAD: 'upload',
  PENCIL: 'pencil',
  MAIL: 'mail'
};

export { IconName };
