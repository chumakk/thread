const ButtonColor = {
  TEAL: 'teal',
  BLUE: 'blue',
  RED: 'red',
  YELLOW: 'yellow',
  GREY: 'grey'
};

export { ButtonColor };
