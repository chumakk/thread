const AppRoute = {
  ROOT: '/',
  ANY: '*',
  LOGIN: '/login',
  FORGOT: '/forgot',
  RESET: '/reset/:token',
  REGISTRATION: '/registration',
  PROFILE: '/profile',
  SHARE_$POSTHASH: '/share/:postHash'
};

export { AppRoute };
