import { AuthApiPath } from '../../common/enums/enums';
import {
  authentication as authenticationMiddleware,
  registration as registrationMiddleware,
  jwt as jwtMiddleware,
  forgotPassword as forgotPasswordMiddleware,
  resetPassword as resetPasswordMiddleware
} from '../../middlewares/middlewares';

const initAuth = (Router, services) => {
  const { auth: authService, user: userService } = services;
  const router = Router();

  // user added to the request (req.user) in a strategy, see passport config
  router
    .post(AuthApiPath.LOGIN, authenticationMiddleware, (req, res, next) => authService
      .login(req.user)
      .then(data => res.send(data))
      .catch(next))
    .post(AuthApiPath.REGISTER, registrationMiddleware, (req, res, next) => authService
      .register(req.user)
      .then(data => res.send(data))
      .catch(next))
    .get(AuthApiPath.USER, jwtMiddleware, (req, res, next) => userService
      .getUserById(req.user.id)
      .then(data => res.send(data))
      .catch(next))
    .put(AuthApiPath.UPDATEUSER, (req, res, next) => userService
      .updateUser(req.user.id, req.body)
      .then(data => res.send(data))
      .catch(next))
    .post(AuthApiPath.FORGOT, forgotPasswordMiddleware, (req, res, next) => authService
      .forgot(req.user)
      .then(obj => res.send(obj))
      .catch(next))
    .get(AuthApiPath.RESET, resetPasswordMiddleware, (req, res) => res.send({}))
    .post(AuthApiPath.RESET, resetPasswordMiddleware, (req, res, next) => authService
      .reset(req.user, req.body)
      .then(obj => res.send(obj))
      .catch(next));

  return router;
};

export { initAuth };
