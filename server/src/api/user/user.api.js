import { UserApiPath } from '../../common/enums/enums';

const initUser = (Router, services) => {
  const { user: userService } = services;
  const router = Router();

  router
    .get(UserApiPath.ROOT, (req, res, next) => userService
      .getUsers()
      .then(users => res.send(users))
      .catch(next));

  return router;
};

export { initUser };
