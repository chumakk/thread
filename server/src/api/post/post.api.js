import { PostsApiPath } from '../../common/enums/enums';
import { sendNotifyOwner, sendSharePost } from '../../config/nodemailer';

const initPost = (Router, services) => {
  const { post: postService } = services;
  const router = Router();

  router
    .get(PostsApiPath.ROOT, (req, res, next) => postService
      .getPosts(req.query, req.user.id)
      .then(posts => res.send(posts))
      .catch(next))
    .get(PostsApiPath.$ID, (req, res, next) => postService
      .getPostById(req.params.id, req.user.id)
      .then(post => res.send(post))
      .catch(next))
    .get(PostsApiPath.LIKES, (req, res, next) => postService
      .getPostLikes(req.query, req.params.id)
      .then(obj => res.send(obj?.postReactions ?? []))
      .catch(next))
    .post(PostsApiPath.ROOT, (req, res, next) => postService
      .create(req.user.id, req.body)
      .then(post => {
        req.io.emit('new_post', post); // notify all users that a new post was created
        return res.send(post);
      })
      .catch(next))
    .post(PostsApiPath.SHARE, (req, res, next) => postService
      .share(req.body, req.user)
      .then(({ email }) => sendSharePost(req.body.postId, email, req.user.username))
      .then(() => res.send({}))
      .catch(next))
    .put(PostsApiPath.UPDATE, (req, res, next) => {
      postService
        .update(req.body, req.user)
        .then(post => res.send(post))
        .catch(next);
    })
    .put(PostsApiPath.REACT, (req, res, next) => postService
      .setReaction(req.user.id, req.body)
      .then(({ post, owner }) => {
        const postObj = post.toJSON();
        if (postObj.reaction !== null && postObj.userId !== req.user.id) {
          if (postObj.reaction) {
            sendNotifyOwner(postObj.id, owner.email, req.user.username);
            req.io
              .to(postObj.userId)
              .emit('like', 'Your post was liked!');
          } else {
            req.io
              .to(postObj.userId)
              .emit('dislike', 'Your post was disliked(');
          }
        }
        return res.send(postObj);
      })
      .catch(next))
    .delete(PostsApiPath.DELETE, (req, res, next) => postService
      .delete(req.body, req.user.id)
      .then(id => res.send({ id }))
      .catch(next));

  return router;
};

export { initPost };
