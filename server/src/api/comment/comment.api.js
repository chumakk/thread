import { CommentsApiPath } from '../../common/enums/enums';

const initComment = (Router, services) => {
  const { comment: commentService } = services;
  const router = Router();

  router
    .get(CommentsApiPath.$ID, (req, res, next) => commentService
      .getCommentById(req.params.id)
      .then(comment => res.send(comment))
      .catch(next))
    .get(CommentsApiPath.LIKES, (req, res, next) => commentService
      .getCommentLikes(req.query, req.params.id)
      .then(obj => res.send(obj?.commentReactions ?? []))
      .catch(next))
    .post(CommentsApiPath.ROOT, (req, res, next) => commentService
      .create(req.user.id, req.body)
      .then(comment => res.send(comment))
      .catch(next))
    .put(CommentsApiPath.UPDATE, (req, res, next) => commentService
      .update(req.user.id, req.body)
      .then(comment => res.send(comment))
      .catch(next))
    .delete(CommentsApiPath.DELETE, (req, res, next) => commentService
      .delete(req.user.id, req.body)
      .then(id => { res.send({ id }); })
      .catch(next))
    .put(CommentsApiPath.REACTION, (req, res, next) => commentService
      .setReaction(req.user.id, req.body)
      .then(reaction => { res.send(reaction); })
      .catch(next));

  return router;
};

export { initComment };
