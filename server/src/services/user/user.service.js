import BadRequestError from '../../common/errors/BadRequestError';

class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  getUsers() {
    return this._userRepository.getUsers();
  }

  async getUserById(id) {
    const user = await this._userRepository.getUserById(id);

    return user;
  }

  async updateUser(authUserId, data) {
    const {
      username,
      imageId,
      status
    } = data;
    if (username) {
      const user = await this._userRepository.getUserById(authUserId);
      const usernameIsExist = await this._userRepository.getByUsername(username);
      if (usernameIsExist && usernameIsExist.id !== user.id) throw new BadRequestError('Username is already taken.');
    }
    await this._userRepository.updateById(authUserId, { username, imageId, status });
    return this._userRepository.getUserById(authUserId);
  }
}

export { User };
