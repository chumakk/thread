import BadRequestError from '../../common/errors/BadRequestError';

class Comment {
  constructor({ commentRepository, commentReactionRepository }) {
    this._commentRepository = commentRepository;
    this._commentReactionRepository = commentReactionRepository;
  }

  async create(userId, comment) {
    const addedComment = await this._commentRepository.create({
      ...comment,
      userId
    });

    return this.getCommentById(addedComment.id);
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }

  getCommentLikes(filter, id) {
    return this._commentRepository.getCommentLikes(filter, id);
  }

  async update(userId, { id, body }) {
    const comment = await this._commentRepository.getCommentById(id);

    if (!comment || comment.userId !== userId) throw new BadRequestError("Can't update comment");

    await this._commentRepository.updateById(id, { body });
    return this._commentRepository.getCommentById(id);
  }

  async delete(userId, { id }) {
    const comment = await this._commentRepository.getCommentById(id);

    if (!comment || comment.userId !== userId) throw new BadRequestError("Can't delete comment");

    await this._commentRepository.updateById(id, { deleted: true });

    return id;
  }

  async setReaction(userId, { commentId, isLike = true }) {
    const updateOrDelete = react => (react.isLike === isLike
      ? this._commentReactionRepository.deleteById(react.id)
      : this._commentReactionRepository.updateById(react.id, { isLike }));

    const reaction = await this._commentReactionRepository.getCommentReaction(
      userId,
      commentId
    );

    if (reaction) {
      await updateOrDelete(reaction);
    } else {
      await this._commentReactionRepository.create({ userId, commentId, isLike });
    }

    return this._commentRepository.getCommentById(commentId);
  }
}

export { Comment };
