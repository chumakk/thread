import BadRequestError from '../../common/errors/BadRequestError';

class Post {
  constructor({ postRepository, postReactionRepository, userRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
    this._userRepository = userRepository;
  }

  getPosts(filter, userId) {
    return this._postRepository.getPosts(filter, userId);
  }

  getPostLikes(filter, postId) {
    return this._postRepository.getLikesOfPost(filter, postId);
  }

  getPostById(id, userId) {
    return this._postRepository.getPostById(id, userId);
  }

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  async update(data, user) {
    const { id, body, imageId = null } = data;
    const post = await this._postRepository.getPostById(id, user.id);
    if (!post || post.userId !== user.id) throw new BadRequestError("Can't update post");

    const updatedPost = await this._postRepository.updateById(id, {
      body,
      imageId
    });

    return this._postRepository.getPostById(updatedPost.id, user.id);
  }

  async delete({ id }, userId) {
    const post = await this._postRepository.getPostById(id, userId);

    if (!post || post.userId !== userId) throw new BadRequestError("Can't delete post");

    await this._postRepository.updateById(id, {
      deleted: true
    });

    return id;
  }

  async setReaction(userId, { postId, isLike = true }) {
    const updateOrDelete = react => (react.isLike === isLike
      ? this._postReactionRepository.deleteById(react.id)
      : this._postReactionRepository.updateById(react.id, { isLike }));

    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    if (reaction) {
      await updateOrDelete(reaction);
    } else {
      await this._postReactionRepository.create({ userId, postId, isLike });
    }
    const post = await this._postRepository.getPostById(postId, userId);
    const owner = await this._userRepository.getUserById(post.userId);
    return { post, owner };
  }

  async share({ userId }, user) {
    if (userId === user.id) throw new BadRequestError("You can't share post with yourself");
    return this._userRepository.getUserById(userId);
  }
}

export { Post };
