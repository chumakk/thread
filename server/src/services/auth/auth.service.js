import crypto from 'crypto';
import { encrypt, createToken } from '../../helpers/helpers';
import { sendResetPassword } from '../../config/nodemailer';

class Auth {
  constructor({ userRepository }) {
    this._userRepository = userRepository;

    this.register = this.register.bind(this);
  }

  async login({ id }) {
    return {
      token: createToken({ id }),
      user: await this._userRepository.getUserById(id)
    };
  }

  async register({ password, ...userData }) {
    const newUser = await this._userRepository.addUser({
      ...userData,
      password: await encrypt(password)
    });

    return this.login(newUser);
  }

  async forgot(user) {
    const random = crypto.randomBytes(20).toString('hex');
    const randomToken = crypto.createHash('sha256').update(random).digest('hex');
    const data = {
      passwordResetToken: randomToken,
      passwordExpiredToken: Date.now() + 3600000
    };

    await this._userRepository.updateById(user.id, data);
    await sendResetPassword(user.email, randomToken);
    return {};
  }

  async reset(user, { password }) {
    const data = {
      password: await encrypt(password),
      passwordResetToken: null,
      passwordExpiredToken: null
    };
    const newUser = await this._userRepository.updateById(user.id, data);

    return this.login(newUser);
  }
}

export { Auth };
