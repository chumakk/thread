import { user as userRepository } from '../../data/repositories/repositories';

const resetPassword = async (req, res, next) => {
  try {
    const user = await userRepository.getByPasswordResetToken(req.params.token);
    if (user) {
      if (Date.parse(user.passwordExpiredToken) - Date.now() >= 0) {
        req.user = user;
        next();
      } else {
        next({ status: 400, message: 'Token expired, try again.' });
      }
    } else {
      next({ status: 404, message: "Token isn't valid, try again." });
    }
  } catch (error) {
    next(error);
  }
};

export { resetPassword };
