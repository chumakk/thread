import { user as userRepository } from '../../data/repositories/repositories';

const forgotPassword = async (req, res, next) => {
  try {
    const user = await userRepository.getByEmail(req.body.email);
    if (user) {
      req.user = user;
      next();
    } else {
      next({ status: 404, message: 'User with this email not found' });
    }
  } catch (error) {
    next(error);
  }
};

export { forgotPassword };
