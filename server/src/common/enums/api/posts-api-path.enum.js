const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  UPDATE: '/update',
  DELETE: '/delete',
  LIKES: '/likes/:id',
  SHARE: '/share'
};

export { PostsApiPath };
