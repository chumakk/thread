const CommentsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  UPDATE: '/update',
  DELETE: '/delete',
  REACTION: '/react',
  LIKES: '/likes/:id'
};

export { CommentsApiPath };
