const AuthApiPath = {
  LOGIN: '/login',
  REGISTER: '/register',
  USER: '/user',
  UPDATEUSER: '/updateuser',
  FORGOT: '/forgot',
  RESET: '/reset/:token'
};

export { AuthApiPath };
