import { DataTypes } from 'sequelize';

const init = orm => {
  const Comment = orm.define(
    'comment',
    {
      body: {
        allowNull: false,
        type: DataTypes.TEXT
      },
      deleted: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
      },
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE
    },
    {}
  );

  return Comment;
};

export { init };
