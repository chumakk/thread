import { sequelize as orm } from '../db/connection';
import associate from '../db/associations';
import { init as initUserModel } from './user/user.model';
import { init as initPostModel } from './post/post.model';
import { init as initPostReactionModel } from './post-reaction/post-reaction.model';
import { init as initCommentModel } from './comment/comment.model';
import { init as initImageModel } from './image/image.model';
import { init as initCommentReactionModel } from './comment-reaction/comment-reaction.model';

const User = initUserModel(orm);
const Post = initPostModel(orm);
const PostReaction = initPostReactionModel(orm);
const Comment = initCommentModel(orm);
const Image = initImageModel(orm);
const CommentReaction = initCommentReactionModel(orm);

associate({
  User,
  Post,
  PostReaction,
  Comment,
  Image,
  CommentReaction
});

export {
  User as UserModel,
  Post as PostModel,
  PostReaction as PostReactionModel,
  Comment as CommentModel,
  Image as ImageModel,
  CommentReaction as CommentReactionModel
};
