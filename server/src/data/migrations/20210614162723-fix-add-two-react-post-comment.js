export default {
  up: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addIndex('commentReactions', ['commentId', 'userId'], { unique: true },
        { transaction }),
      queryInterface.addIndex('postReactions', ['userId', 'postId'], { unique: true },
        { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeIndex('commentReactions', ['commentId', 'userId'], { transaction }),
      queryInterface.removeIndex('postReactions', ['userId', 'postId'], { transaction })
    ]))
};
