export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('users', 'passwordResetToken', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.addColumn('users', 'passwordExpiredToken', {
        type: Sequelize.DATE
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('users', 'passwordResetToken', { transaction }),
      queryInterface.removeColumn('users', 'passwordExpiredToken', { transaction })
    ]))
};
