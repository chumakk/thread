export default models => {
  const {
    User,
    Post,
    PostReaction,
    Comment,
    Image,
    CommentReaction
  } = models;

  Image.hasOne(User);
  Image.hasOne(Post);

  User.hasMany(Post);
  User.hasMany(Comment);
  User.hasMany(PostReaction);
  User.hasMany(CommentReaction, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  });
  User.belongsTo(Image);

  Post.belongsTo(Image);
  Post.belongsTo(User);
  Post.hasMany(PostReaction);
  Post.hasMany(Comment);

  Comment.hasMany(CommentReaction, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  });
  Comment.belongsTo(User);
  Comment.belongsTo(Post);

  PostReaction.belongsTo(Post);
  PostReaction.belongsTo(User);

  CommentReaction.belongsTo(Comment, {
    foreignKey: {
      allowNull: false
    }
  });
  CommentReaction.belongsTo(User, {
    foreignKey: {
      allowNull: false
    }
  });
};
