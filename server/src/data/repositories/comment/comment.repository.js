import { Abstract } from '../abstract/abstract.repository';
import { sequelize } from '../../db/connection';

const likeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class Comment extends Abstract {
  constructor({ commentModel, userModel, imageModel, commentReactionModel }) {
    super(commentModel);
    this._userModel = userModel;
    this._imageModel = imageModel;
    this._commentReactionModel = commentReactionModel;
  }

  getCommentById(id) {
    return this.model.findOne({
      group: ['comment.id', 'user.id', 'user->image.id'],
      where: { id, deleted: false },
      attributes: {
        include: [
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ],
        exclude: ['deleted']
      },
      include: [
        {
          model: this._userModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: this._commentReactionModel,
          attributes: []
        }
      ]
    });
  }

  getCommentLikes(filter, commentId) {
    const {
      from: offset,
      count: limit
    } = filter;

    const whereReactions = { isLike: true, commentId };
    return this.model.findOne({
      attributes: [],
      include: [
        {
          model: this._commentReactionModel,
          where: { ...whereReactions },
          attributes: ['isLike'],
          include: {
            model: this._userModel,
            attributes: ['id', 'username'],
            include: {
              model: this._imageModel,
              attributes: ['id', 'link']
            }
          },
          duplicating: false
        }
      ],
      group: [
        'comment.id',
        'commentReactions.id',
        'commentReactions->user.id',
        'commentReactions->user->image.id'
      ],
      order: [['commentReactions', 'userId', 'ASC']],
      offset,
      limit
    });
  }
}

export { Comment };
