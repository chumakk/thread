import { Op } from 'sequelize';
import { sequelize } from '../../db/connection';
import { Abstract } from '../abstract/abstract.repository';

const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class Post extends Abstract {
  constructor({
    postModel,
    commentModel,
    userModel,
    imageModel,
    postReactionModel,
    commentReactionModel
  }) {
    super(postModel);
    this._commentModel = commentModel;
    this._userModel = userModel;
    this._imageModel = imageModel;
    this._postReactionModel = postReactionModel;
    this._commentReactionModel = commentReactionModel;
  }

  async getPosts(filter, authUserId) {
    const {
      from: offset,
      count: limit,
      userId,
      noUserId,
      reaction
    } = filter;
    const where = { deleted: false };

    if (userId) {
      Object.assign(where, { userId });
    }

    if (noUserId) {
      Object.assign(where, { userId: { [Op.ne]: noUserId } });
    }

    const whereReactions = {};
    if (reaction) {
      Object.assign(whereReactions, { isLike: reaction, userId: authUserId });
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deleted" = false)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount'],
          [sequelize.literal(`(SELECT "postReactions"."isLike"
                              FROM "postReactions"
                              WHERE "postReactions"."userId" = '${authUserId}'
                              AND "postReactions"."postId"="post"."id" LIMIT 1 ) `), 'reaction']
        ],
        exclude: ['deleted']
      },
      include: [{
        model: this._imageModel,
        attributes: ['id', 'link']
      }, {
        model: this._userModel,
        attributes: ['id', 'username'],
        include: {
          model: this._imageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: this._postReactionModel,
        where: { ...whereReactions },
        required: Boolean(Object.keys(whereReactions).length),
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getLikesOfPost(filter, postId) {
    const {
      from: offset,
      count: limit
    } = filter;

    const whereReactions = { isLike: true, postId };
    return this.model.findOne({
      attributes: [],
      include: [
        {
          model: this._postReactionModel,
          where: { ...whereReactions },
          attributes: ['isLike'],
          include: {
            model: this._userModel,
            attributes: ['id', 'username'],
            include: {
              model: this._imageModel,
              attributes: ['id', 'link']
            }
          },
          duplicating: false
        }
      ],
      group: [
        'post.id',
        'postReactions.id',
        'postReactions->user.id',
        'postReactions->user->image.id'
      ],
      order: [['postReactions', 'userId', 'ASC']],
      offset,
      limit
    });
  }

  getPostById(id, authUserId) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id, deleted: false },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deleted" = false)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount'],
          [sequelize.literal(`(SELECT "postReactions"."isLike"
                              FROM "postReactions"
                              WHERE "postReactions"."userId" = '${authUserId}'
                              AND "postReactions"."postId"="post"."id" LIMIT 1) `), 'reaction']
        ],
        exclude: ['deleted']
      },
      include: [{
        model: this._commentModel,
        required: false,
        where: { deleted: false },
        attributes: {
          include: [
            [sequelize.literal(`
            (SELECT COUNT(*)
            FROM "commentReactions" as "commentReaction"
            WHERE "comments"."id" = "commentReaction"."commentId" AND "commentReaction"."isLike" = true)`),
            'likeCount'],
            [sequelize.literal(`
            (SELECT COUNT(*)
            FROM "commentReactions" as "commentReaction"
            WHERE "comments"."id" = "commentReaction"."commentId" AND "commentReaction"."isLike" = false)`),
            'dislikeCount']
          ],
          exclude: ['deleted']
        },
        include: [{
          model: this._userModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        }, {
          model: this._commentReactionModel,
          attributes: []
        }
        ]
      }, {
        model: this._userModel,
        attributes: ['id', 'username'],
        include: {
          model: this._imageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: this._imageModel,
        attributes: ['id', 'link']
      }, {
        model: this._postReactionModel,
        attributes: []
      }
      ]
    });
  }
}

export { Post };
