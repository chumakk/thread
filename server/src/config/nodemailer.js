import nodemailer from 'nodemailer';
import { ENV } from '../common/enums/enums';

const options = {
  host: 'smtp-relay.sendinblue.com',
  port: 587,
  auth: {
    user: ENV.SENDINBLUE.USER,
    pass: ENV.SENDINBLUE.PASSWORD
  }
};

const { FROM } = ENV.SENDINBLUE;

const transporter = nodemailer.createTransport(options);

const sendResetPassword = async (email, token) => {
  const subject = 'Reset Password Thread-JS';
  const href = `http://localhost:3000/reset/${token}`;
  const html = `<p>Click link below to reset the password</p>
    <p>${href}</p>`;

  return transporter.sendMail({
    from: FROM,
    to: email,
    subject,
    html
  });
};

const sendNotifyOwner = async (postId, email, username) => {
  const subject = 'Your post was liked on Thread-JS';
  const href = `http://localhost:3000/share/${postId}`;
  const html = `<p>Hello!</p>
                <p>Your post was liked by ${username} </p>
                <p>Post: ${href}</p>`;

  return transporter.sendMail({
    from: FROM,
    to: email,
    subject,
    html
  });
};

const sendSharePost = (postId, email, username) => {
  const subject = `${username} shared post with you on Thread-JS`;
  const href = `http://localhost:3000/share/${postId}`;
  const html = `<p>Hello!</p>
                <p>${username} shared post with you on Thread-JS</p>
                <p>Post: ${href}</p>`;

  return transporter.sendMail({
    from: FROM,
    to: email,
    subject,
    html
  });
};

export { sendResetPassword, sendNotifyOwner, sendSharePost };
